﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using Repository.Account;
using Repository.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IRoleRepository _roleRepository;

        public AccountController(
            ILogger<AccountController> logger,
            IConfiguration configuration,
            IAccountRepository accountRepository,
            IRoleRepository roleRepository
            ) : base(configuration, logger)
        {
            _accountRepository = accountRepository;
            _roleRepository = roleRepository;
        }

        [Route("Authencate")]
        [HttpPost]
        public Response<string> Authencate(LoginModel model)
        {
            Response<string> res = new Response<string>();

            var rs = _accountRepository.Authencate(model);
            if (rs.Code != StatusCodes.Status200OK)
            {
                res.Code = StatusCodes.Status401Unauthorized;
                res.Message = rs.Message;
                return res;
            }
            var role = _roleRepository.GetRoleByUserId(rs.Data.Id);
            var newToken = _accountRepository.EncodeSha256(rs.Data, role.Data.UserRoleName);
            res.Data = newToken;
            if (!string.IsNullOrEmpty(model.Token))
                res.Code = StatusCodes.Status200OK;
            return res;
        }

    }
}
