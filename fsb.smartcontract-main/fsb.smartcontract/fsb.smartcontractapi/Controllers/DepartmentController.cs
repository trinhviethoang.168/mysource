﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model.Department;
using Model.Requests;
using Model.Response;
using Repository.Department;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : BaseController
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentController(
            IDepartmentRepository departmentRepository,
            Microsoft.Extensions.Logging.ILogger<DepartmentController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _departmentRepository = departmentRepository;
        }

        [Route("GetListDepartment")]
        [HttpPost]
        public TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search)
        {
            return _departmentRepository.GetListDepartment(search);
        }

        [Route("CreateDepartment")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> CreateDepartment(DepartmentModel model)
        {
            return _departmentRepository.CreateDepartment(model);
        }

        [Route("DeleteDepartment")]
        [HttpPost]
        public Response<string> DeleteDepartment(DepartmentModel model)
        {
            return _departmentRepository.DeleteDepartment(model);
        }

    }
}
