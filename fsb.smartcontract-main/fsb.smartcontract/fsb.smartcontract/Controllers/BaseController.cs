﻿using Common;
using Common.Constants;
using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Model.Requests;
using Model.Response;
using Model.UserInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace fsb.smartcontract.Controllers
{
    public class BaseController : Controller
    {
        private IHttpClientFactory _factory;

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            string authId = GenerateAuthId();

            //Store the value in both our Session and a Cookie.
            //HttpContext.Session.SetString("AuthId", authId);
            //CookieOptions options = new CookieOptions()
            //{
            //    Path = "/",
            //    HttpOnly = true,
            //    Secure = true,
            //    IsEssential = true,
            //    SameSite = SameSiteMode.None
            //};
            //context.HttpContext.Response.Cookies.Append("AuthCookie", authId);
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var path = context.HttpContext.Request.Path.Value;

            if (!path.ToLower().Contains("account/actionlogin") && path != "/")
            {
                string sessionValue = HttpContext.Session.GetString("AuthId");
                string cookieValue = Request.Cookies["AuthCookie"];
                if (cookieValue == null || sessionValue == null || cookieValue != sessionValue)
                {
                    if (context.HttpContext.IsAjaxRequest())
                    {
                        Response<string> res = new Response<string>();
                        res.Code = StatusCodes.Status401Unauthorized;
                        res.Message = "Token invalid!";
                        res.Data = "/";
                        context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(res).ToLower());
                    }
                    context.Result = new RedirectResult("/");
                    //clearCookie();
                    return;
                }

                var keys = HttpContext.Session.Keys.Count();
                if (keys == 0)
                {
                    if (context.HttpContext.IsAjaxRequest())
                    {
                        Response<string> res = new Response<string>();
                        res.Code = StatusCodes.Status401Unauthorized;
                        res.Message = "Token invalid!";
                        res.Data = "/";
                        context.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(res).ToLower());
                    }
                    context.Result = new RedirectResult("/");
                    //clearCookie();
                    return;
                }
            }
        }
        public BaseController(
            IHttpClientFactory factory)
        {
            _factory = factory;
        }
        protected void clearCookie()
        {
            if (HttpContext.Request.Cookies.Count > 0)
            {
                var siteCookies = HttpContext.Request.Cookies.Where(c => c.Key.Contains("Session"));
                foreach (var cookie in siteCookies)
                {
                    Response.Cookies.Delete(cookie.Key);
                }
            }

            HttpContext.Session.Clear();
            string authId = GenerateAuthId();

            // Store the value in both our Session and a Cookie.
            HttpContext.Session.SetString("AuthId", authId);
            CookieOptions options = new CookieOptions()
            {
                Path = "/",
                HttpOnly = true,
                Secure = true,
                SameSite = SameSiteMode.Strict
            };
            Response.Cookies.Append("AuthCookie", authId, options);
        }
        protected string GenerateAuthId()
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[32];
                rng.GetBytes(tokenData);
                return Convert.ToBase64String(tokenData);
            }
        }
        public string token
        {
            get
            {
                try
                {
                    var ss = HttpContext.Session.GetObject<string>(CommonConstants.TOKEN);
                    return ss;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                CallWebAPI._token = value;
                HttpContext.Session.SetObject<string>(CommonConstants.TOKEN, value);
            }
        }

        public UserViewModel _SessionUser
        {
            get
            {
                try
                {
                    var ss = HttpContext.Session.GetObject<UserViewModel>(LoginConstant.PERSON_INFO_SESSION);
                    return ss;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Session.SetObject<UserViewModel>(LoginConstant.PERSON_INFO_SESSION, value);
            }
        }
        public Config _Config
        {
            get
            {
                try
                {
                    var ss = HttpContext.Session.GetObject<Config>(LoginConstant.CONFIG);
                    return ss;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Session.SetObject<Config>(LoginConstant.CONFIG, value);
            }
        }
        public TableSearchModel talbeSearchModel
        {
            get
            {
                TableSearchModel search = new TableSearchModel();
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                var orderColumn = Request.Form["order[0][column]"].FirstOrDefault();
                var orderDir = Request.Form["order[0][dir]"].FirstOrDefault();
                search.Draw = int.Parse(draw);
                search.Start = int.Parse(start);
                search.Length = int.Parse(length);
                search.SearchValue = searchValue;
                search.OrderColumn = int.Parse(orderColumn);
                search.OrderDir = orderDir;
                return search;
            }
        }
        protected string ip
        {
            get
            {

                try
                {
                    return Request.HttpContext.Connection.RemoteIpAddress.ToString();
                }
                catch (Exception ex)
                {
                }

                return null;
            }
        }
        protected IActionResult RedirectToErrorPage()
        {
            return RedirectToAction("Error", "Home");
        }
        public static string GetPlainText(string fileName)
        {
            fileName = HttpUtility.HtmlDecode(fileName).Replace(@"-", " ")
                         .Replace(@"\", "")
                         .Replace("<script>", "").Replace("</script>", "")
                         .Replace("svg", "").Replace("<", "")
                         .Replace(">", "").Replace("#", "")
                         .Replace("$", "")
                         .Replace("%", "").Replace("^", "")
                         .Replace("&", "").Replace("*", "")
                         .Replace("{", "").Replace("}", "")
                         .Replace(";", "").Replace("alert", "")
                         .Replace("spam", "").Replace("+", "")
                         .Replace("'", "").Replace("(", "")
                         .Replace(")", "").Replace("\"", "")
                         .Replace("=", "");
            return fileName;
        }
    }
}
