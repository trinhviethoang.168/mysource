﻿using Model.Company;
using Model.Requests;
using Model.Response;

namespace fsb.smartcontract.Services.Company
{
    public interface ICompanyService
    {
        TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search);
        Response<string> CreateCompany(CompanyModel model);
        Response<string> DeleteCompany(CompanyModel model);
        Response<CompanyModel> GetCompanyById(CompanyModel model);
        Response<string> UpdateCompany(CompanyModel model);
        dynamic GetListCompanyForCombo();
    }
}
