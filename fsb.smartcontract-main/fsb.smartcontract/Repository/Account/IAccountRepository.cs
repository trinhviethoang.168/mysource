﻿using Entity.Entity;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Account
{
    public interface IAccountRepository
    {
        string EncodeSha256(User user, string role);

        Response<User> Authencate(LoginModel model);
    }
}
